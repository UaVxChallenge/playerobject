package com.src.player;

public class ComparePoints extends Player implements Comparable<Player>{

	public ComparePoints(String name, int points, int power) {
		super(name, points, power);
	}

	@Override
	public int compareTo(Player o) {
		
		if(this.getPoints() > o.getPoints()) return 1;
		if(this.getPoints() == o.getPoints()) return 0;
		if(this.getPoints() < o.getPoints()) return -1;
		
		return 0;
	}
}
