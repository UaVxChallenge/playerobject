package com.src.player;

public class Player implements Comparable<Player>{
	
	public int Power;
	public int Points;
	public String Name;
	
	public Player(String name, int points, int power){

		Name = name;
		
		Points = points;
		
		Power = power;
		
	}
	
	public String getName(){
		return Name;
	}
	
	public void setPoints(int i){

		Points = i;

	}
	
	public int getPoints(){
		return Points;
	}
	
	public void setPower(int i){
		
		Power = i;
			
	}
	
	public int getPower(){
		return Power;
	}

	@Override
	public int compareTo(Player o) {
		
		int a = this.getName().substring(this.getName().length() - 1).compareTo(o.getName().substring(o.getName().length() - 1));
		
		if(a > 0){
			//o.getName will return first!
			return -1;
		}
		if(a == 0){
			//Last letter of names are equal!
			return 0;
		}
		if(a < 0){
			//this.Name will return as first alphabetically from last letter!
			return 1;
		}
		
		return 1;
	}
	
}


