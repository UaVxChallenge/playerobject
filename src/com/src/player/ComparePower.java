package com.src.player;

public class ComparePower extends Player implements Comparable<Player>{

	public ComparePower(String name, int points, int power) {
		super(name, points, power);
	}

	@Override
	public int compareTo(Player o) {
		
		if(this.getPower() > o.getPower()) return 1;
		if(this.getPower() == o.getPower()) return 0;
		if(this.getPower() < o.getPower()) return -1;
		
		return 0;
	}

}
